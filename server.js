var express=require("express");
	mongoose=require("mongoose");
	routes=require("./server/routes/routes");


var app=express();
app.use(express.static('public'));

mongoose.Promise=global.Promise;
mongoose.connect('mongodb://localhost/passlab');

routes(app);
app.listen(3000);

