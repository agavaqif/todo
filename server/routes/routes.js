var gwitt=require("../models/models.js");
var bodyParser=require("body-parser");


var urlencodedParser = bodyParser.urlencoded({ extended: false })
var jsonParser = bodyParser.json()

module.exports=function(app){
app.post("/api/message",jsonParser,function(req,res){
	
	gwitt(req.body).save(function(err,data){
		if(err) throw err;
		res.json(data);
	})
});

app.get("/api/message",function(req,res){
	gwitt.find({},function(err,data){
		if(err) throw err;
		res.json(data);
	})
});

app.delete("/api/message/:id",function(req,res){
	
	gwitt.remove({_id:req.params.id},function(err,data){
		if(err) throw err;
		res.json(data);
	})
});

	app.put("/api/message/:id",jsonParser,function(req,res){

		gwitt.update({_id:req.params.id},{$set:{message:req.body.info}},function(err,data){
			if(err) throw err;
			res.json(data);
		})
	})
}
