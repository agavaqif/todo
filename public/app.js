myApp=angular.module("myApp",['ngRoute']);



myApp.config( ['$routeProvider', function($routeProvider) {
  $routeProvider.when("/home",{
    templateUrl:"views/home.html",
  }).when("/todo",{
    templateUrl:"views/todo.html",
    controller:"todoController"
  })
  .otherwise({
    redirecTo:"views/home.html"
  });
}]);
